package com.example.ypp.sericefeign.controller;

import com.example.ypp.sericefeign.service.SchedualServiceHi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by cuizhongcheng on 2017/12/6
 * email : cuizhongcheng@yupaopao.cn
 */
@RestController
public class HiController {
    @Autowired
    SchedualServiceHi schedualServiceHi;

    /**
     * 对外暴露一个”/hi”的API接口，通过定义的Feign客户端SchedualServiceHi 来消费服务
     * @param name
     * @return
     */
    @RequestMapping(value = "/hi",method = RequestMethod.GET)
    public String sayHi(@RequestParam String name){
        return schedualServiceHi.sayHiFromClientOne(name);
    }
}
