package com.example.ypp.sericefeign.service;

import org.springframework.stereotype.Component;

/**
 * Created by cuizhongcheng on 2017/12/6
 * email : cuizhongcheng@yupaopao.cn
 */
@Component
public class SchedualServiceHiHystric implements SchedualServiceHi{
    @Override
    public String sayHiFromClientOne(String name) {
        return "sorry "+name;
    }
}
