package com.example.ypp.sericefeign.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by cuizhongcheng on 2017/12/6
 * email : cuizhongcheng@yupaopao.cn
 */
@FeignClient(value = "service-hi",fallback = SchedualServiceHiHystric.class)
public interface SchedualServiceHi {

    /**
     * 定义一个feign接口，通过@ FeignClient（“服务名”），来指定调用哪个服务
     * 这里的value参数hi是调用服务里的hi接口
     * @param name
     * @return
     */
    @RequestMapping(value = "/hi",method = RequestMethod.GET)
    String sayHiFromClientOne(@RequestParam(value = "name") String name);

}
