package com.example.ypp.serviceribbon.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by cuizhongcheng on 2017/12/6
 * email : cuizhongcheng@yupaopao.cn
 */
@Service
public class HelloService {
    @Autowired
    RestTemplate restTemplate;

    /**
     * 注入ioc容器的restTemplate来消费service-hi服务的“/hi”接口
     * 接用的程序名替代了具体的url地址，在ribbon中它会根据服务名来选择具体的服务实例
     *
     * 加上@HystrixCommand注解，创建熔断器，指定fallbackMethod熔断方法
     * @param name
     * @return
     */
    @HystrixCommand(fallbackMethod = "hiError")
    public String hiService(String name) {
        return restTemplate.getForObject("http://SERVICE-HI/hi?name="+name,String.class);
    }

    /**
     * 拦截java.lang.IllegalStateException
     * @param name
     * @return
     */
    public String hiError(String name) {
        return "hi,"+name+",sorry,error!";
    }

}
